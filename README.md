# README #

Iplementação da uma classe de pilha simples de inteiros que possui os métodos push, pop e min. 
Onde min retorna o menor inteiro na pilha e todos os métodos são O(1).

### Tools ###

* Java - 1.8.0_72
* Ide:IntelliJ IDEA Community - https://www.jetbrains.com/idea/download/#section=mac
* Junit4

