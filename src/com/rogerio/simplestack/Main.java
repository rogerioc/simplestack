package com.rogerio.simplestack;

import com.rogerio.simplestack.exception.NoneElementException;
import com.rogerio.simplestack.mystack.MyStack;

/**
 * Created by rogerio on 09/10/17.
 * Iplementação da uma classe de pilha de inteiros que possui os métodos push, pop e min.
 * Onde min retorna o menor inteiro na pilha e todos os métodos são O(1).
 */
public class Main {
    public static void main(String args[])
    {
        MyStack myStack = new MyStack();
        myStack.push(4);
        myStack.push(10);
        myStack.push(10);


        try {
            System.out.print(myStack.min());
        } catch (NoneElementException e) {
            e.printStackTrace();
        }
    }

}
