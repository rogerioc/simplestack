package com.rogerio.simplestack.mystack;

import com.rogerio.simplestack.exception.EmptyElementException;
import com.rogerio.simplestack.exception.NoneElementException;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by rogerio on 09/10/17.
 * Iplementação da uma classe de pilha de inteiros que possui os métodos push, pop e min.
 * Onde min retorna o menor inteiro na pilha e todos os métodos são O(1).
 */
public class MyStack {
    int indexLastElement =0;
    int minId=-1;
    Stack<Integer> stack=new Stack<>();
    public void push(int element) {
        if(stack.empty())
            minId=element;
        else if(minId>element) {
            int aux = element;
            element = 2*element - minId;
            minId = aux;
        }
        stack.push(element);
    }

    public int min() throws NoneElementException {
        return minId;
    }
    public int pop() throws EmptyElementException {
        if(!stack.isEmpty()) {
            int element = stack.pop();
            if(element<minId) {
                int aux = minId;
                minId = 2*minId - element;
                element = aux;
            }
            return element;
        }
        else
            throw new EmptyElementException();

    }

    public int size() {
        return stack.size();
    }
}
