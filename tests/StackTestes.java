import com.rogerio.simplestack.exception.EmptyElementException;
import com.rogerio.simplestack.exception.NoneElementException;
import com.rogerio.simplestack.mystack.MyStack;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by rogerio on 09/10/17.
 */
public class StackTestes {
    MyStack myStack;
    @Before
    public void setup(){
        myStack = new MyStack();
    }

    @Test
    public void pushPopElement() throws EmptyElementException {
        int element = 1;
        myStack.push(element);
        assert(myStack.size()==1);
        assert(myStack.pop()==element);
        assert(myStack.size()==0);
    }

    @Test
    public void push2PopElement() throws EmptyElementException {
        myStack.push(100);
        myStack.push(1);
        myStack.push(2);
        assert (myStack.size() == 3);
        assertEquals(myStack.pop(),2);
        assertEquals(myStack.pop(),1);
        assertEquals(myStack.pop(),100);

    }
        @Test
    public void pushMinElement() throws NoneElementException {
        int element = 1;
        myStack.push(element);
        assertEquals(myStack.min(),element);
    }

    @Test
    public void foundMinElement() throws NoneElementException {
        int element = 1;
        myStack.push(element);
        myStack.push(100);
        myStack.push(2);
        myStack.push(4);
        myStack.push(2000);
        assertEquals(myStack.min(),element);
    }

    @Test
    public void foundMinMiddleElement() throws NoneElementException {
        int element = 1;
        myStack.push(100);
        myStack.push(2);
        myStack.push(element);
        myStack.push(4);
        myStack.push(2000);
        assertEquals(myStack.min(),element);
    }

    @Test
    public void foundMinElementPop() throws NoneElementException, EmptyElementException {
        int element = 1;
        int secMinElement = 2;
        myStack.push(100);
        myStack.push(secMinElement);
        myStack.push(4);
        myStack.push(2000);
        myStack.push(element);
        myStack.pop();
        assertEquals(myStack.min(),secMinElement);
    }

    @Test
    public void noneMinElement() throws EmptyElementException {
        int element = 1;
        myStack.push(element);
        myStack.pop();

        try {
            myStack.min();
        } catch (NoneElementException e) {
            assert(true);
        }
    }

    @Test
    public void noneElement() {
        try {
            myStack.pop();
        } catch (EmptyElementException e) {
            assert(true);
        }
    }
}
